package com.antashev.currencyapp.ui

import com.antashev.currencyapp.domain.CurrencyRateDownloader
import com.antashev.currencyapp.domain.CurrencyType
import com.antashev.currencyapp.domain.RxRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Flowable
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal

class CurrencyPresenterTest {
    @JvmField
    @Rule
    val rule = RxRule()

    @Test
    fun startCurrencyRateUpdating_ByDefault() {
        val downloader = mock<CurrencyRateDownloader> {
            on { download(CurrencyType.AUD) } doReturn Flowable.just(mapOf("EUR" to BigDecimal.TEN))
        }
        val view = mock<CurrencyView>()
        val presenter = createPresenter(downloader = downloader)
        presenter.attachView(view)

        presenter.changeBaseCurrency(CurrencyType.AUD)

        verify(view, times(1)).updateCurrencies(mapOf("EUR" to BigDecimal.TEN))
    }

    @Test
    fun startCurrencyRateUpdating_ThrowException() {
        val downloader = mock<CurrencyRateDownloader> {
            on { download(CurrencyType.AUD) } doReturn Flowable.error(Exception())
        }
        val view = mock<CurrencyView>()
        val presenter = createPresenter(downloader = downloader)
        presenter.attachView(view)

        presenter.changeBaseCurrency(CurrencyType.AUD)

        verify(view, times(1)).showError("Unknown error")
    }

    private fun createPresenter(downloader: CurrencyRateDownloader = mock()): CurrencyPresenter {
        return CurrencyPresenter(downloader)
    }
}