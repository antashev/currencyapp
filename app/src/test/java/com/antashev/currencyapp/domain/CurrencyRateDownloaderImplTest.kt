package com.antashev.currencyapp.domain

import com.antashev.currencyapp.network.CurrencyApi
import com.antashev.currencyapp.network.CurrencyDto
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import java.time.Instant.now
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class CurrencyRateDownloaderImplTest {
    private val scheduler = TestScheduler()
    @JvmField
    @Rule
    val rule = RxRule(scheduler)

    @Test
    fun download_Error() {
        val api = mock<CurrencyApi> {
            on { getCurrencyRate("AUD") } doReturn Single.error(Exception())
        }
        val downloader = createDownloader(api = api)
        val testSubscriber = downloader.download(CurrencyType.AUD).test()
        assertTrue(testSubscriber.isTerminated)
    }

    @Test
    fun download_CallFewTimes() {
        val dto =
            CurrencyDto(base = CurrencyType.AUD.name, date = Date.from(now()), rates = HashMap())
        val api = mock<CurrencyApi> {
            on { getCurrencyRate("AUD") } doReturn Single.just(dto)
        }
        val downloader = createDownloader(api = api)
        val testSubscriber = downloader.download(CurrencyType.AUD).test()
        testSubscriber.assertValueCount(1)
        scheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        testSubscriber.assertValueCount(2)
        testSubscriber.dispose()
    }

    private fun createDownloader(api: CurrencyApi = mock()): CurrencyRateDownloader {
        return CurrencyRateDownloaderImpl(api)
    }
}