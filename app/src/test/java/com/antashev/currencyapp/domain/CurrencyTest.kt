package com.antashev.currencyapp.domain

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import java.math.BigDecimal

class CurrencyTest {

    @Test
    fun calculateAmount_ScaleDown() {
        val amount =
            Currency(BigDecimal("1.56789"), CurrencyType.AUD).calculateAmount(BigDecimal("10"))
        assertThat(amount, equalTo(BigDecimal("15.67")))
    }
}