package com.antashev.currencyapp.di

interface ComponentHolder {
    val component: AppComponent
}