package com.antashev.currencyapp.di

import com.antashev.currencyapp.ui.CurrencyActivityComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component
interface AppComponent {
    fun currencyActivityComponent(): CurrencyActivityComponent
}