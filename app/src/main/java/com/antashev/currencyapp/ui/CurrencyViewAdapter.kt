package com.antashev.currencyapp.ui

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.antashev.currencyapp.R
import com.antashev.currencyapp.domain.Currency
import com.antashev.currencyapp.domain.CurrencyType
import kotlinx.android.synthetic.main.item_currency.view.*
import java.math.BigDecimal

class CurrencyViewAdapter : RecyclerView.Adapter<CurrencyViewAdapter.CurrencyViewHolder>() {

    companion object {
        const val UPDATE_RATE_PAYLOAD = 1
    }

    var onCurrencyClicked: (currency: Currency) -> Unit = { }
    private var multiplier: BigDecimal = BigDecimal("1.00")
    private val currencies: ArrayList<Currency> =
        ArrayList(CurrencyType.values().map { Currency(rate = BigDecimal.ZERO, type = it) })
    init {
        currencies[0].rate = BigDecimal("1.00")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_currency, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun getItemCount() = currencies.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencies[position])
    }

    override fun onBindViewHolder(
        holder: CurrencyViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isNotEmpty()) {
            holder.bindWithPayloads(currencies[position], payloads)
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    fun update(rates: Map<String, BigDecimal>) {
        currencies.forEach { it.rate = rates[it.type.name] ?: it.rate }
        notifyItemRangeChanged(1, currencies.size - 1, UPDATE_RATE_PAYLOAD)
    }

    fun onBaseCurrencyChanged(position: Int) {
        val currency = currencies.removeAt(position)
        currencies.add(0, currency)
        currency.rate = BigDecimal("1.00")
        notifyItemMoved(position, 0)
        notifyItemChanged(0)
    }

    fun getBaseCurrency(): CurrencyType {
        return currencies[0].type
    }

    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            //ignored
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            //ignored
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s.isNullOrEmpty()) {
                multiplier = BigDecimal.ZERO
            } else {
                multiplier = BigDecimal(s.toString())
            }
            notifyItemRangeChanged(1, currencies.size - 1, UPDATE_RATE_PAYLOAD)
        }
    }

    inner class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(currency: Currency) {
            itemView.title.text = currency.type.name
            itemView.description.text = currency.type.description
            itemView.flag.setImageResource(currency.type.flagRes)
            showAmount(currency)
            if (adapterPosition == 0) {
                configureBaseCurrency()
            } else {
                configureCommonCurrency()
            }
        }

        fun bindWithPayloads(currency: Currency, payloads: MutableList<Any>) {
            if (payloads[0] == UPDATE_RATE_PAYLOAD) {
                showAmount(currency)
            }
        }

        private fun showAmount(currency: Currency) {
            val amount = currency.calculateAmount(multiplier).toString()
            itemView.amount.removeTextChangedListener(textWatcher)
            itemView.amount.setText(amount)
        }

        private fun configureBaseCurrency() {
            itemView.amount.addTextChangedListener(textWatcher)
            itemView.setOnClickListener(null)
            itemView.amount.requestFocus()
        }

        private fun configureCommonCurrency() {
            itemView.setOnClickListener {
                onCurrencyClicked(currencies[adapterPosition])
                multiplier = BigDecimal(itemView.amount.text.toString())
                onBaseCurrencyChanged(adapterPosition)
            }
        }
    }
}