package com.antashev.currencyapp.ui

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.antashev.currencyapp.R
import com.antashev.currencyapp.di.ComponentHolder
import com.antashev.currencyapp.domain.CurrencyRateDownloader
import com.antashev.currencyapp.network.NetworkModule
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_currency.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Provider

class CurrencyActivity : MvpAppCompatActivity(), CurrencyView {
    @Inject
    lateinit var presenterProvider: Provider<CurrencyPresenter>
    @InjectPresenter
    lateinit var presenter: CurrencyPresenter
    @ProvidePresenter
    fun providePresenter(): CurrencyPresenter {
        return presenterProvider.get()
    }

    private val viewAdapter = CurrencyViewAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as ComponentHolder)
            .component
            .currencyActivityComponent()
            .inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)

        val linearLayoutManager = LinearLayoutManager(this)
        currencyList.adapter = viewAdapter
        currencyList.layoutManager = linearLayoutManager
        currencyList.setHasFixedSize(true)
        viewAdapter.onCurrencyClicked = { currency ->
            Timber.i("Base currency selected: $currency")
            presenter.changeBaseCurrency(currency.type)
            linearLayoutManager.scrollToPositionWithOffset(0, 0)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.startCurrencyRateUpdating(viewAdapter.getBaseCurrency())
    }

    override fun onPause() {
        super.onPause()
        presenter.stopCurrencyRateUpdating()
    }

    override fun updateCurrencies(rates: Map<String, BigDecimal>) {
        viewAdapter.update(rates)
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}

@Subcomponent(modules = [CurrencyActivityModule::class])
interface CurrencyActivityComponent {
    fun inject(activity: CurrencyActivity)
}

@Module(includes = [NetworkModule::class])
class CurrencyActivityModule {
    @Provides
    fun provideCurrencyPresenter(rateDownloader: CurrencyRateDownloader): CurrencyPresenter {
        return CurrencyPresenter(rateDownloader)
    }
}
