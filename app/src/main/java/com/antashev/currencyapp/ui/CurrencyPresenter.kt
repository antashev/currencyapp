package com.antashev.currencyapp.ui

import com.antashev.currencyapp.domain.CurrencyRateDownloader
import com.antashev.currencyapp.domain.CurrencyType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import timber.log.Timber

@InjectViewState
class CurrencyPresenter(
    private val rateDownloader: CurrencyRateDownloader
): MvpPresenter<CurrencyView>() {
    private var disposable: Disposable? = null

    fun startCurrencyRateUpdating(currencyType: CurrencyType) {
        disposable = rateDownloader.download(currencyType)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ rates ->
                viewState.updateCurrencies(rates)
            }, { error ->
                Timber.e(error, "Get currency rate by server")
                viewState.showError(error.message ?: "Unknown error")
            })
    }

    fun stopCurrencyRateUpdating() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
    }

    fun changeBaseCurrency(currencyType: CurrencyType) {
        stopCurrencyRateUpdating()
        startCurrencyRateUpdating(currencyType)
    }
}