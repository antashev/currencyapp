package com.antashev.currencyapp.ui

import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType
import java.math.BigDecimal

interface CurrencyView : MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun updateCurrencies(rates: Map<String, BigDecimal>)

    @StateStrategyType(SkipStrategy::class)
    fun showError(message: String)
}