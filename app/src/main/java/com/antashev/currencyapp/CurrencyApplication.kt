package com.antashev.currencyapp

import android.app.Application
import com.antashev.currencyapp.di.AppComponent
import com.antashev.currencyapp.di.ComponentHolder
import com.antashev.currencyapp.di.DaggerAppComponent
import timber.log.Timber

class CurrencyApplication : Application(), ComponentHolder {

    override val component: AppComponent by lazy {
        DaggerAppComponent.builder()
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        configureTimber()
    }

    private fun configureTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}