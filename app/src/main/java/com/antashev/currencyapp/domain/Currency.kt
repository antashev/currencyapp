package com.antashev.currencyapp.domain

import java.math.BigDecimal
import java.math.RoundingMode

data class Currency(
    var rate: BigDecimal,
    val type: CurrencyType
) {
    fun calculateAmount(multiplier: BigDecimal): BigDecimal {
        return rate.multiply(multiplier).setScale(2, RoundingMode.DOWN)
    }
}