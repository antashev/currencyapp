package com.antashev.currencyapp.domain

import androidx.annotation.DrawableRes
import com.antashev.currencyapp.R

enum class CurrencyType(@DrawableRes val flagRes: Int, val description: String) {
    EUR(R.drawable.ic_eur, "Euro"),
    AUD(R.drawable.ic_aud, "Australian Dollar"),
    BGN(R.drawable.ic_bgn, "Bulgarian Lev"),
    BRL(R.drawable.ic_brl, "Brazilian Real"),
    CAD(R.drawable.ic_cad, "Canadian Dollar"),
    CHF(R.drawable.ic_chf, "Swiss Franc"),
    CNY(R.drawable.ic_cny, "Chinese Yuan"),
    CZK(R.drawable.ic_czk, "Czech Koruna"),
    DKK(R.drawable.ic_dkk, "Danish Krone"),
    GBP(R.drawable.ic_gbp, "British Pound"),
    HKD(R.drawable.ic_hkd, "Hong Kong Dollar"),
    HRK(R.drawable.ic_hrk, "Croatian Kuna"),
    HUF(R.drawable.ic_huf, "Hungarian Forint"),
    IDR(R.drawable.ic_idr, "Indonesian Rupiah"),
    ILS(R.drawable.ic_ils, "Israeli Shekel"),
    INR(R.drawable.ic_inr, "Indian Rupee"),
    ISK(R.drawable.ic_isk, "Icelandic Krona"),
    JPY(R.drawable.ic_jpy, "Japanese Yen"),
    KRW(R.drawable.ic_krw, "South Korean Won"),
    MXN(R.drawable.ic_mxn, "Mexican Peso"),
    MYR(R.drawable.ic_myr, "Malaysian Ringgit"),
    NOK(R.drawable.ic_nok, "Norwegian Krone"),
    NZD(R.drawable.ic_nzd, "New Zealand Dollar"),
    PHP(R.drawable.ic_php, "Philippine Peso"),
    PLN(R.drawable.ic_pln, "Polish Zloty"),
    RON(R.drawable.ic_ron, "Danish Krone"),
    RUB(R.drawable.ic_rub, "Russian Ruble"),
    SEK(R.drawable.ic_sek, "Swedish Krona"),
    SGD(R.drawable.ic_sgd, "Singapore Dollar"),
    THB(R.drawable.ic_thb, "Thai Baht"),
    TRY(R.drawable.ic_try, "Turkish Lira"),
    USD(R.drawable.ic_usd, "US Dollar"),
    ZAR(R.drawable.ic_zar, "South African Rand")
}