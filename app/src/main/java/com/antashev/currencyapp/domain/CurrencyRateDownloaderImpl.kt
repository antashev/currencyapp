package com.antashev.currencyapp.domain

import com.antashev.currencyapp.network.CurrencyApi
import io.reactivex.Flowable
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class CurrencyRateDownloaderImpl(
    private val api: CurrencyApi
) : CurrencyRateDownloader {
    override fun download(baseCurrency: CurrencyType): Flowable<Map<String, BigDecimal>> {
        return api.getCurrencyRate(baseCurrency.name)
            .repeatWhen { t -> t.delay(1, TimeUnit.SECONDS) }
            .onBackpressureDrop()
            .map { it.rates }
    }
}