package com.antashev.currencyapp.domain

import io.reactivex.Flowable
import java.math.BigDecimal

interface CurrencyRateDownloader {
    /**
     * Download currency rates map once per second
     * @param baseCurrency - basic Currency with rate 1.00
     * @return map.key - [CurrencyType.name] map.value - currency rate value
     */
    fun download(baseCurrency: CurrencyType): Flowable<Map<String, BigDecimal>>
}