package com.antashev.currencyapp.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface CurrencyApi {
    @GET("latest") // ?base=EUR
    fun getCurrencyRate(@Query("base") baseCurrency: String): Single<CurrencyDto>
}