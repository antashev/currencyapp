package com.antashev.currencyapp.network

import java.math.BigDecimal
import java.util.*
import kotlin.collections.HashMap

data class CurrencyDto(
    val base: String,
    val date: Date,
    val rates: HashMap<String, BigDecimal>
)

