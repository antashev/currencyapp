package com.antashev.currencyapp.network

import com.antashev.currencyapp.domain.CurrencyRateDownloader
import com.antashev.currencyapp.domain.CurrencyRateDownloaderImpl
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {
    @Provides
    fun provideCurrencyApi(): CurrencyApi {
        return Retrofit
            .Builder()
            .baseUrl("https://revolut.duckdns.org/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(OkHttpClient())
            .build()
            .create(CurrencyApi::class.java)
    }

    @Provides
    fun provideCurrencyRateDownloader(api: CurrencyApi): CurrencyRateDownloader {
        return CurrencyRateDownloaderImpl(api)
    }
}